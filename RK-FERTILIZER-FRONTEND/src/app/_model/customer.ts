import { Location } from './location';

export class Customer {
  customerID: number;
  customerName: string;
  location: Location;
  phoneNumber: string;
}
