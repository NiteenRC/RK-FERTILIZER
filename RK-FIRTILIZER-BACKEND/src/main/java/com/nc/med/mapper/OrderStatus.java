package com.nc.med.mapper;

public enum OrderStatus {
	FULLY_PAID, PARTIAL_PAID, NOT_PAID
}
